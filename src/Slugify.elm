module Slugify exposing (..)


slugify : String -> String
slugify s =
    s
        |> String.toLower
        |> String.map
            (\c ->
                case c of
                    '-' ->
                        ' '

                    '_' ->
                        ' '

                    '\'' ->
                        ' '

                    '"' ->
                        ' '

                    otherCharacter ->
                        otherCharacter
            )
        |> String.words
        |> List.map
            (\fragment ->
                case fragment of
                    "1er" ->
                        "1"

                    "ier" ->
                        "i"

                    "premier" ->
                        "1"

                    otherFragment ->
                        otherFragment
            )
        |> String.join "-"
