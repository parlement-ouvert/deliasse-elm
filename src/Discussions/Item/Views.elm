module Discussions.Item.Views exposing (..)

import Configuration
import Dict
import Discussions.Item.Routes exposing (..)
import Discussions.Item.Types exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onWithOptions)
import Json.Encode
import Json.Decode
import Material.Button as Button
import Material.Icon as Icon
import Material.Layout as Layout
import Material.Options as Options exposing (css)
import Material.Progress as Loading
import Material.Snackbar as Snackbar
import Material.Typography as Typo
import Regex exposing (HowMany(..), caseInsensitive, find, regex)
import Set
import Slugify exposing (slugify)
import Types exposing (..)


aForPath : String -> List (Attribute Msg) -> List (Html Msg) -> Html Msg
aForPath path attributes children =
    a
        ([ href path
         , onWithOptions
            "click"
            { stopPropagation = True, preventDefault = True }
            (Json.Decode.succeed (ForParent <| Navigate path))
         ]
            ++ attributes
        )
        children


layoutLinkForHash : String -> List (Options.Style Msg) -> List (Html Msg) -> Html Msg
layoutLinkForHash hash attributes children =
    Layout.link
        ([ Layout.href ("#" ++ hash)
         , Options.attribute <|
            onWithOptions
                "click"
                { stopPropagation = True, preventDefault = True }
                (Json.Decode.succeed (ForSelf <| ScrollToTop hash))
         ]
            ++ attributes
        )
        children


layoutLinkForPath : String -> List (Options.Style Msg) -> List (Html Msg) -> Html Msg
layoutLinkForPath path attributes children =
    Layout.link
        ([ Layout.href path
         , Options.attribute <|
            onWithOptions
                "click"
                { stopPropagation = True, preventDefault = True }
                (Json.Decode.succeed (ForParent <| Navigate path))
         ]
            ++ attributes
        )
        children


header : Model -> List (Html Msg)
header model =
    [ Layout.row
        [ Options.nop
        , css "transition" "height 333ms ease-in-out 0s"
        ]
        [ Layout.title [] [ text Configuration.appTitle ]
        , Layout.spacer
        , Layout.navigation []
            ((case model.discussion of
                Just (Err message) ->
                    []

                Just (Ok discussion) ->
                    case discussion.numeroProchainADiscuter of
                        Just numeroProchainADiscuter ->
                            [ layoutLinkForHash (slugify numeroProchainADiscuter)
                                [ Options.attribute <| title "Accéder à l'amendement en cours de discussion." ]
                                [ Icon.i "whatshot" ]
                            ]

                        Nothing ->
                            []

                Nothing ->
                    []
             )
                ++ [ Layout.link
                        [ Layout.href "https://donnees-personnelles.parlement-ouvert.fr/" ]
                        [ text "Dossier législatif" ]
                   ]
            )
        ]
    ]


shortTitleFromGroupe : Groupe -> String
shortTitleFromGroupe groupe =
    case groupe of
        GaucheDémocrateEtRépublicaine ->
            "Gauche démocrate et républicaine"

        LaFranceInsoumise ->
            "La France insoumise"

        LaRépubliqueEnMarche ->
            "La République en Marche"

        LesRépublicains ->
            "Les Républicains"

        MouvementDémocrateEtApparentés ->
            "Mouvement Démocrate et apparentés"

        NonInscrits ->
            "Non inscrits"

        NouvelleGauche ->
            "Nouvelle Gauche"

        PasUnGroupe ->
            "~~~"

        -- Gouvernement...
        UdiAgirEtIndépendants ->
            "UDI, Agir et Indépendants"


titleFromAuteurGroupe : Auteur -> Groupe -> String
titleFromAuteurGroupe auteur groupe =
    case groupe of
        GaucheDémocrateEtRépublicaine ->
            "Groupe de la Gauche démocrate et républicaine"

        LaFranceInsoumise ->
            "Groupe La France insoumise"

        LaRépubliqueEnMarche ->
            "Groupe La République en Marche"

        LesRépublicains ->
            "Groupe Les Républicains"

        MouvementDémocrateEtApparentés ->
            "Groupe du Mouvement Démocrate et apparentés"

        NonInscrits ->
            if auteur.civilite == "Mme" then
                "Non inscrite"
            else
                "Non inscrit"

        NouvelleGauche ->
            "Groupe Nouvelle Gauche"

        PasUnGroupe ->
            ""

        -- Gouvernement...
        UdiAgirEtIndépendants ->
            "Groupe UDI, Agir et Indépendants"


view : Model -> Html Msg
view model =
    case model.discussion of
        Just (Err message) ->
            p [] [ text message ]

        Just (Ok discussion) ->
            div [ class "discussion" ]
                [ Options.styled h1
                    [ Typo.center
                    , Typo.display2
                    ]
                    [ text discussion.titre ]
                , case model.route of
                    ByGroupeRoute ->
                        viewByGroupe model discussion

                    FullRoute ->
                        viewFull model discussion

                    IndexRoute ->
                        viewIndex model discussion
                , Snackbar.view model.snackbar |> Html.map (ForSelf << Snackbar)
                ]

        Nothing ->
            Loading.indeterminate


viewAmendement : Model -> Discussion -> EnveloppeAmendement -> Html Msg
viewAmendement model discussion enveloppeAmendement =
    case enveloppeAmendement.amendement of
        Just amendement ->
            let
                pageSlug =
                    enveloppeAmendement.place
                        |> find
                            (AtMost 1)
                            (caseInsensitive <| regex "(article|chapitre|titre)\\s+(\\d+|[cilvx]+|1er|Ier|premier)(\\s+([a-f]|bis|ter|quater|quinquies))?")
                        |> List.head
                        |> Maybe.map .match
                        |> Maybe.map slugify

                page =
                    case pageSlug of
                        Just pageSlug ->
                            Dict.get pageSlug discussion.pages

                        Nothing ->
                            Nothing
            in
                article
                    [ class "amendementBlock"
                    , id (slugify enveloppeAmendement.numero)
                    ]
                    ([ case page of
                        Just page ->
                            Button.render (ForSelf << Mdl)
                                [ 0 ]
                                model.mdl
                                [ Button.fab
                                , Button.ripple
                                , Options.onClick (ForSelf <| OpenLinkedContent amendement page)
                                , css "float" "right"
                                , Options.cs "btn-linked-content"
                                , Options.attribute <| title "Lire l'article du projet de loi."
                                ]
                                [ Icon.i "library_books" ]

                        Nothing ->
                            text ""
                     , div
                        [ class "amendementEntete"
                        , style [ ( "margin-bottom", "30px" ) ]
                        ]
                        [ h3
                            [ style
                                [ ( "text-align", "left" )
                                , ( "font-size", "34px" )
                                ]
                            ]
                            [ span
                                [ style
                                    [ ( "display", "block" )
                                    , ( "font-size", "16px" )
                                    , ( "color", "grey" )
                                    , ( "padding", "10px 0px 10px 0px" )
                                    , ( "text-decoration", "none" )
                                    ]
                                ]
                                [ text enveloppeAmendement.place ]
                            , text ("Amendement " ++ amendement.numeroLong)
                            , case discussion.numeroProchainADiscuter of
                                Just numeroProchainADiscuter ->
                                    if numeroProchainADiscuter == enveloppeAmendement.numero then
                                        span []
                                            [ text " "
                                            , Icon.view "whatshot"
                                                [ css "color" "orange"
                                                , css "font-size" "34px"
                                                , css "padding-bottom" "15px"
                                                ]
                                            ]
                                    else
                                        text ""

                                Nothing ->
                                    text ""
                            , viewSortEnSeance amendement.sortEnSeance
                            ]
                        ]
                     ]
                        ++ (if String.isEmpty amendement.dispositif then
                                []
                            else
                                [ div [ class "amendementInstructions" ]
                                    [ h4 [] [ text "Titre" ]
                                    , div
                                        [ amendement.dispositif
                                            |> Json.Encode.string
                                            |> Html.Attributes.property "innerHTML"
                                        ]
                                        []
                                    ]
                                ]
                           )
                        ++ (if String.isEmpty amendement.exposeSommaire then
                                []
                            else
                                [ div [ class "exposeSommaire" ]
                                    [ h4 [] [ text "Exposé sommaire" ]
                                    , div
                                        [ amendement.exposeSommaire
                                            |> Json.Encode.string
                                            |> Html.Attributes.property "innerHTML"
                                        ]
                                        []
                                    ]
                                ]
                           )
                        ++ (let
                                auteur =
                                    amendement.auteur
                            in
                                if auteur.estGouvernement then
                                    [ div [ class "amendement-author" ]
                                        [ img
                                            [ class "gvt-logo"
                                            , src "/logo-gouvernement.png"
                                            , alt "logo du gouvernement"
                                            , title "logo du gouvernement"
                                            ]
                                            []
                                        , p [ class "author-info" ]
                                            [ text "Amendement proposé par le Gouvernement." ]
                                        ]
                                    ]
                                else
                                    [ div [ class "amendement-author" ]
                                        [ img
                                            [ src auteur.photoUrl
                                            , alt
                                                ("Portrait de "
                                                    ++ auteur.civilite
                                                    ++ " "
                                                    ++ auteur.prenom
                                                    ++ " "
                                                    ++ auteur.nom
                                                )
                                            , title
                                                (auteur.prenom
                                                    ++ " "
                                                    ++ auteur.nom
                                                )
                                            ]
                                            []
                                        , div [ class "author-info" ]
                                            [ strong []
                                                [ text
                                                    (auteur.civilite
                                                        ++ " "
                                                        ++ auteur.prenom
                                                        ++ " "
                                                        ++ auteur.nom
                                                        ++ (Maybe.withDefault "" auteur.qualite)
                                                        ++ (case amendement.cosignatairesMentionLibre of
                                                                Just cosignatairesMentionLibre ->
                                                                    " " ++ cosignatairesMentionLibre.titre

                                                                Nothing ->
                                                                    ""
                                                           )
                                                    )
                                                ]
                                            , br [] []

                                            -- , text "Sarthe (2ème Circonscription)"
                                            -- , br [] []
                                            , text <| titleFromAuteurGroupe auteur enveloppeAmendement.auteurGroupe
                                            ]
                                        ]
                                    ]
                           )
                        ++ (if List.isEmpty amendement.cosignataires then
                                []
                            else
                                [ h4 [] [ text (toString (List.length amendement.cosignataires) ++ " Co-signataires :") ]
                                , ul [ class "amendement-cosignataires" ]
                                    (amendement.cosignataires
                                        |> List.map
                                            (\cosignataire ->
                                                li []
                                                    [ strong []
                                                        [ text
                                                            (cosignataire.civilite
                                                                ++ " "
                                                                ++ cosignataire.prenom
                                                                ++ " "
                                                                ++ cosignataire.nom
                                                                ++ (Maybe.withDefault "" cosignataire.qualite)
                                                            )
                                                        ]
                                                    ]
                                            )
                                    )
                                ]
                           )
                    )

        Nothing ->
            article
                [ class "amendementBlock"
                , id (slugify enveloppeAmendement.numero)
                ]
                [ div [ class "amendementEntete" ]
                    [ h3 [] [ text ("Amendement n° " ++ enveloppeAmendement.numero) ]
                    , viewSortEnSeance enveloppeAmendement.sort
                    ]
                , div [ class "amendementInstructions" ]
                    [ Loading.indeterminate ]
                ]


viewByGroupe : Model -> Discussion -> Html Msg
viewByGroupe model discussion =
    let
        groupeAuteurCouples =
            discussion.amendements
                |> List.map
                    (\enveloppeAmendement -> ( enveloppeAmendement.auteurGroupe, enveloppeAmendement.auteurLabel ))

        groupeByShortTitle =
            groupeAuteurCouples
                |> List.map (\( groupe, _ ) -> ( shortTitleFromGroupe groupe, groupe ))
                |> Dict.fromList

        groupesShortTitles =
            groupeByShortTitle
                |> Dict.keys
                |> List.sort

        -- x =
        --     List.sort groupeCouples
        -- |> Set.fromList
        -- groupeCouples =
        --     groupes
        --         |> Set.toList
        --         |> List.map
        --             (\groupe -> ( shortTitleFromGroupe groupe, groupe ))
    in
        div []
            (groupesShortTitles
                |> List.map
                    (\groupeShortTitle ->
                        section []
                            ([ Options.styled h2
                                [ Typo.center
                                , Typo.display1
                                ]
                                [ text groupeShortTitle ]
                             ]
                                ++ (let
                                        groupe =
                                            Dict.get groupeShortTitle groupeByShortTitle
                                                |> Maybe.withDefault PasUnGroupe

                                        auteurs =
                                            groupeAuteurCouples
                                                |> List.filter (\( groupe1, auteur ) -> groupe1 == groupe)
                                                |> List.map (\( _, auteur ) -> auteur)
                                                |> Set.fromList
                                                |> Set.toList
                                                |> List.sort
                                    in
                                        auteurs
                                            |> List.map
                                                (\auteur ->
                                                    section []
                                                        ([ Options.styled h3
                                                            [ Typo.center
                                                            , Typo.headline
                                                            ]
                                                            [ text auteur ]
                                                         ]
                                                            ++ (discussion.amendements
                                                                    |> List.filter
                                                                        (\enveloppeAmendement -> enveloppeAmendement.auteurLabel == auteur)
                                                                    |> List.map
                                                                        (\enveloppeAmendement ->
                                                                            let
                                                                                slug =
                                                                                    slugify enveloppeAmendement.numero
                                                                            in
                                                                                article []
                                                                                    [ aForPath ("tout#" ++ slug)
                                                                                        [ id slug
                                                                                        , style
                                                                                            [ ( "color", "#231479" )
                                                                                            , ( "text-decoration", "none" )
                                                                                            ]
                                                                                        ]
                                                                                        [ Options.styled span
                                                                                            [ Typo.headline ]
                                                                                            [ text ("Amendement " ++ enveloppeAmendement.numero) ]
                                                                                        , case discussion.numeroProchainADiscuter of
                                                                                            Just numeroProchainADiscuter ->
                                                                                                if numeroProchainADiscuter == enveloppeAmendement.numero then
                                                                                                    span []
                                                                                                        [ text " "
                                                                                                        , Icon.view "whatshot"
                                                                                                            [ css "color" "orange"
                                                                                                            , css "font-size" "34px"
                                                                                                            , css "padding-bottom" "15px"
                                                                                                            ]
                                                                                                        ]
                                                                                                else
                                                                                                    text ""

                                                                                            Nothing ->
                                                                                                text ""
                                                                                        , viewSortEnSeance enveloppeAmendement.sort
                                                                                        ]
                                                                                    ]
                                                                        )
                                                               )
                                                        )
                                                )
                                   )
                            )
                    )
            )


viewDrawer : Model -> List (Html Msg)
viewDrawer model =
    case model.discussion of
        Just (Err message) ->
            []

        Just (Ok discussion) ->
            let
                path =
                    "/" ++ model.bibard ++ "/" ++ model.bibardSuffixe ++ "/" ++ toString (model.legislature) ++ "/" ++ model.organe
            in
                [ Layout.title [] [ Options.styled p [ Typo.title, css "margin-top" "10px" ] [ text discussion.titre ] ]
                , Layout.navigation []
                    [ layoutLinkForPath path [] [ text "Index chronologique des amendements" ]
                    , layoutLinkForPath (path ++ "/" ++ "groupes") [] [ text "Index des amendements par auteur" ]
                    , layoutLinkForPath (path ++ "/" ++ "tout") [] [ text "Tous les amendements" ]
                    ]
                ]

        Nothing ->
            [ Loading.indeterminate ]


viewFull : Model -> Discussion -> Html Msg
viewFull model discussion =
    div []
        (discussion.divisions
            |> List.map
                (\division ->
                    section []
                        ([ Options.styled h2
                            [ Options.id (slugify division.place)
                            , Typo.center
                            , Typo.display1
                            ]
                            [ text division.place ]
                         ]
                            ++ (discussion.amendements
                                    |> List.filter
                                        (\enveloppeAmendement -> enveloppeAmendement.place == division.place)
                                    |> List.map
                                        (\enveloppeAmendement ->
                                            viewAmendement
                                                model
                                                discussion
                                                enveloppeAmendement
                                        )
                               )
                        )
                )
        )


viewIndex : Model -> Discussion -> Html Msg
viewIndex model discussion =
    div []
        (discussion.divisions
            |> List.map
                (\division ->
                    section []
                        ([ Options.styled h2
                            [ Options.id (slugify division.place)
                            , Typo.center
                            , Typo.display1

                            -- , css "padding" "30px 0px 10px 0px"
                            ]
                            [ text division.place ]
                         ]
                            ++ (discussion.amendements
                                    |> List.filter
                                        (\enveloppeAmendement -> enveloppeAmendement.place == division.place)
                                    |> List.map
                                        (\enveloppeAmendement ->
                                            article
                                                [ id (slugify enveloppeAmendement.numero)
                                                ]
                                                [ Options.styled span
                                                    [ Typo.headline ]
                                                    [ text ("Amendement " ++ enveloppeAmendement.numero) ]
                                                , case discussion.numeroProchainADiscuter of
                                                    Just numeroProchainADiscuter ->
                                                        if numeroProchainADiscuter == enveloppeAmendement.numero then
                                                            span []
                                                                [ text " "
                                                                , Icon.view "whatshot"
                                                                    [ css "color" "orange"
                                                                    , css "font-size" "34px"
                                                                    , css "padding-bottom" "15px"
                                                                    ]
                                                                ]
                                                        else
                                                            text ""

                                                    Nothing ->
                                                        text ""
                                                , viewSortEnSeance enveloppeAmendement.sort
                                                ]
                                        )
                               )
                        )
                )
        )


viewLinkedContent : Model -> Html Msg
viewLinkedContent model =
    case model.discussion of
        Just (Err message) ->
            text ""

        Just (Ok discussion) ->
            case model.billPageModel of
                Just billPageModel ->
                    article [ class "overlay" ]
                        [ Button.render (ForSelf << Mdl)
                            [ 0 ]
                            model.mdl
                            [ Button.fab
                            , Button.colored
                            , Options.onClick (ForSelf <| CloseLinkedContent)
                            , css "margin" "30px 30px 0px 0px"
                            , css "position" "fixed"
                            , css "right" "0px"
                            , css "top" "0px"
                            , css "z-index" "10"
                            ]
                            [ Icon.i "close" ]
                        , div [ class "overlay-content" ]
                            [ h3 [] [ text billPageModel.billPage.numero ]
                            , case billPageModel.billPage.titre of
                                Just titre ->
                                    h4 [] [ text titre ]

                                Nothing ->
                                    text ""
                            , div
                                [ billPageModel.billPage.body
                                    |> Json.Encode.string
                                    |> Html.Attributes.property "innerHTML"
                                ]
                                []
                            ]
                        ]

                Nothing ->
                    text ""

        Nothing ->
            text ""


viewSortEnSeance : SortEnSeance -> Html Msg
viewSortEnSeance sortEnSeance =
    mark
        [ class
            (case sortEnSeance of
                Déposé ->
                    "approvalStatusFiled"

                Adopté ->
                    "approvalStatusAdopted"

                _ ->
                    "approvalStatusRejected"
            )
        ]
        [ text
            (case sortEnSeance of
                Adopté ->
                    "Adopté"

                Déposé ->
                    "À discuter"

                NonSoutenu ->
                    "Non soutenu"

                Rejeté ->
                    "Rejeté"

                Retiré ->
                    "Retiré"

                Tombé ->
                    "Tombé"
            )
        ]
