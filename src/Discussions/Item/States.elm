module Discussions.Item.States exposing (..)

import Decoders
import Discussions.Item.Routes exposing (..)
import Discussions.Item.Types exposing (..)
import Dom.Scroll
import Json.Decode
import Material
import Material.Helpers exposing (map1st, map2nd)
import Material.Snackbar as Snackbar
import Navigation
import Process
import Ports
import Slugify exposing (slugify)
import Task
import Time
import Urls


init : String -> String -> Int -> String -> Model
init bibard bibardSuffixe legislature organe =
    { bibard = bibard
    , bibardSuffixe = bibardSuffixe
    , billPageModel = Nothing
    , discussion = Nothing
    , hash = ""
    , legislature = legislature
    , mdl = Material.model
    , organe = organe
    , route = IndexRoute
    , snackbar = Snackbar.model
    }


sameInit : Model -> Model -> Bool
sameInit left right =
    (left.bibard == right.bibard)
        && (left.bibardSuffixe == right.bibardSuffixe)
        && (left.legislature == right.legislature)
        && (left.organe == right.organe)


subscriptions : Model -> Sub InternalMsg
subscriptions model =
    [ Ports.gotDiscussion GotDiscussion
    ]
        |> Sub.batch


update : InternalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AmendementUpserted amendement ->
            case model.discussion of
                Just (Err message) ->
                    ( model, Cmd.none )

                Just (Ok discussion) ->
                    if
                        (amendement.bibard == model.bibard)
                            && (amendement.bibardSuffixe == model.bibardSuffixe)
                            && (amendement.legislature == model.legislature)
                            && (amendement.organeAbrv == model.organe)
                    then
                        let
                            enveloppesAmendements =
                                discussion.amendements
                                    |> List.map
                                        (\enveloppeAmendement ->
                                            if enveloppeAmendement.numero == amendement.numeroReference then
                                                { enveloppeAmendement
                                                    | amendement = Just amendement
                                                    , sort = amendement.sortEnSeance
                                                }
                                            else
                                                enveloppeAmendement
                                        )

                            newDiscussion =
                                { discussion | amendements = enveloppesAmendements }
                        in
                            ( { model | discussion = Just (Ok newDiscussion) }
                            , Cmd.none
                            )
                    else
                        ( model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        CloseLinkedContent ->
            ( { model | billPageModel = Nothing }
            , Cmd.none
            )

        GotDiscussion discussionJson ->
            let
                discussionResult =
                    Json.Decode.decodeValue Decoders.discussionDecoder discussionJson
            in
                { model | discussion = Just discussionResult }
                    ! [ case discussionResult of
                            Err message ->
                                let
                                    _ =
                                        Debug.log "GotDiscussion Decode error:" message

                                    _ =
                                        Debug.log "GotDiscussion JSON:" discussionJson
                                in
                                    Ports.setDocumentMetadata
                                        { description = "Discussion"
                                        , imageUrl = Urls.appLogoFullUrl
                                        , title = "Discussion"
                                        }

                            Ok discussion ->
                                Ports.setDocumentMetadata
                                    { description = "Amendements pour " ++ discussion.titre
                                    , imageUrl = Urls.appLogoFullUrl
                                    , title = discussion.titre
                                    }
                      , let
                            id =
                                model.hash
                        in
                            -- if String.isEmpty id then
                            --     Cmd.none
                            -- else
                            Task.attempt
                                (always (ForSelf <| ScrolledToId))
                                -- (Dom.Scroll.toTop id)
                                (Process.sleep (5 * Time.second)
                                    |> Task.andThen
                                        (\_ ->
                                            let
                                                _ =
                                                    Debug.log "bou!!"
                                            in
                                                Dom.Scroll.toTop "article-2"
                                        )
                                )
                      ]

        Mdl msg_ ->
            Material.update (ForSelf << Mdl) msg_ model

        OpenLinkedContent amendement billPage ->
            ( { model | billPageModel = Just { amendement = amendement, billPage = billPage } }
            , Cmd.none
            )

        ProchainADiscuterUpserted prochainADiscuter ->
            if
                (prochainADiscuter.bibard == model.bibard)
                    && (prochainADiscuter.bibardSuffixe == model.bibardSuffixe)
                    && (prochainADiscuter.legislature == model.legislature)
                    && (prochainADiscuter.organeAbrv == model.organe)
            then
                let
                    discussion =
                        case model.discussion of
                            Just (Err message) ->
                                Just (Err message)

                            Just (Ok discussion) ->
                                Just
                                    (Ok
                                        { discussion
                                            | numeroProchainADiscuter = Just prochainADiscuter.numAmdt
                                        }
                                    )

                            Nothing ->
                                Nothing

                    ( snackbar, effect ) =
                        Snackbar.add
                            (Snackbar.snackbar
                                prochainADiscuter
                                ("Amendement " ++ prochainADiscuter.numAmdt ++ " en discussion !")
                                "VOIR"
                            )
                            model.snackbar
                            |> map2nd (Cmd.map (ForSelf << Snackbar))
                in
                    ( { model
                        | discussion = discussion
                        , snackbar = snackbar
                      }
                    , effect
                    )
            else
                ( model, Cmd.none )

        ScrolledToId ->
            let
                _ =
                    Debug.log "ScrolledToId" 314
            in
                ( model, Cmd.none )

        ScrollToTop hash ->
            ( model
            , Task.attempt
                (always (ForSelf <| ScrolledToId))
                (Dom.Scroll.toTop hash)
            )

        Snackbar (Snackbar.Begin prochainADiscuter) ->
            ( model, Cmd.none )

        Snackbar (Snackbar.End prochainADiscuter) ->
            ( model, Cmd.none )

        Snackbar (Snackbar.Click prochainADiscuter) ->
            let
                id =
                    slugify prochainADiscuter.numAmdt

                _ =
                    Debug.log "Snackbar id" id
            in
                ( { model | hash = id }
                , Navigation.newUrl ("#" ++ id)
                )

        Snackbar msg_ ->
            Snackbar.update msg_ model.snackbar
                |> map1st (\s -> { model | snackbar = s })
                |> map2nd (Cmd.map (ForSelf << Snackbar))


urlUpdate : Navigation.Location -> Route -> Model -> ( Model, Cmd Msg )
urlUpdate location route model =
    let
        hash =
            String.dropLeft 1 location.hash

        targetId =
            if String.isEmpty hash then
                "html-element"
            else
                hash
    in
        { model
            | hash = hash
            , route = route
        }
            ! [ Task.attempt
                    (always (ForSelf <| ScrolledToId))
                    -- (Dom.Scroll.toTop targetId)
                    (Dom.Scroll.toTop "article-2")
              , Ports.getDiscussion model.bibard model.bibardSuffixe model.legislature model.organe
              ]
