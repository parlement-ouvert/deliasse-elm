module Discussions.Item.Types exposing (..)

import Discussions.Item.Routes exposing (..)
import Json.Encode
import Material
import Material.Snackbar as Snackbar
import Types exposing (..)


type alias BillPageModel =
    { amendement : Amendement
    , billPage : BillPage
    }


type ExternalMsg
    = Navigate String


type InternalMsg
    = AmendementUpserted Amendement
    | CloseLinkedContent
    | GotDiscussion Json.Encode.Value
    | Mdl (Material.Msg Msg)
    | OpenLinkedContent Amendement BillPage
    | ProchainADiscuterUpserted ProchainADiscuter
    | ScrolledToId
    | ScrollToTop String
    | Snackbar (Snackbar.Msg ProchainADiscuter)


type alias Model =
    { bibard : String
    , bibardSuffixe : String
    , billPageModel : Maybe BillPageModel
    , discussion : Maybe (Result String Discussion)
    , hash : String
    , legislature : Int
    , mdl : Material.Model
    , organe : String
    , route : Route -- A page is not needed, because the routes have no model.
    , snackbar : Snackbar.Model ProchainADiscuter
    }


type Msg
    = ForParent ExternalMsg
    | ForSelf InternalMsg


type alias MsgTranslation parentMsg =
    { onInternalMsg : InternalMsg -> parentMsg
    , onNavigate : String -> parentMsg
    }


type alias MsgTranslator parentMsg =
    Msg -> parentMsg


translateMsg : MsgTranslation parentMsg -> MsgTranslator parentMsg
translateMsg { onInternalMsg, onNavigate } msg =
    case msg of
        ForParent (Navigate path) ->
            onNavigate path

        ForSelf internalMsg ->
            onInternalMsg internalMsg
