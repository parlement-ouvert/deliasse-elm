module Discussions.Item.Routes exposing (..)


type Route
    = ByGroupeRoute
    | FullRoute
    | IndexRoute
