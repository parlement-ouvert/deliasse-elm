import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getOperationAST } from 'graphql';
import gql from 'graphql-tag';

import './main.css';
import { Main } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';

var flags = {};
var main = Main.embed(document.getElementById('root'), flags);

registerServiceWorker();


// Ports


// GraphQL
// Cf https://medium.com/@michaelcbrook/how-to-get-apollo-2-0-working-with-graphql-subscriptions-321388be030c
let graphqlClient = null
let graphqAmendementUpsertedSubscription = null
let graphqProchainADiscuterUpsertedSubscription = null

const amendementFragment = gql`
  fragment AmendementFragment on Amendement {
    auteur {
      ...AuteurFragment
    }
    bibard
    bibardSuffixe
    cosignataires {
      ...AuteurFragment
    }
    cosignatairesMentionLibre {
      ...CosignatairesMentionLibreFragment
    }
    dispositif
    etat
    exposeSommaire
    legislature
    numero
    numeroLong
    numeroReference
    organeAbrv
    place
    placeReference
    sortEnSeance
  }
`
const auteurFragment = gql`
  fragment AuteurFragment on Auteur {
    civilite
    estRapporteur
    estGouvernement
    groupeTribunId
    nom
    photoUrl
    prenom
    qualite
    tribunId
  }
`
const cosignatairesMentionLibreFragment = gql`
  fragment CosignatairesMentionLibreFragment on CosignatairesMentionLibre {
    titre
  }
`
const discussionFragment = gql`
  fragment DiscussionFragment on Discussion {
    amendements {
      ...EnveloppeAmendementFragment
    }
    bibard
    bibardSuffixe
    divisions {
      ...DivisionFragment
    }
    legislature
    numeroProchainADiscuter
    organe
    pages {
      ...PageFragment
    }
    titre
    type
  }
`
const divisionFragment = gql`
  fragment DivisionFragment on Division {
    # amendements
    place
    position
  }
`
const enveloppeAmendementFragment = gql`
  fragment EnveloppeAmendementFragment on EnveloppeAmendement {
    alineaLabel
    amendement {
      ...AmendementFragment
    }
    auteurGroupe
    auteurLabel
    discussionCommune
    discussionCommuneAmdtPositon
    discussionCommuneSsAmdtPositon
    discussionIdentique
    discussionIdentiqueAmdtPositon
    discussionIdentiqueSsAmdtPositon
    missionLabel
    numero
    parentNumero
    place
    position
    sort
  }
`
const pageFragment = gql`
  fragment PageFragment on Page {
    body
    numero
    slug
    titre
  }
`
const prochainADiscuterFragment = gql`
  fragment ProchainADiscuterFragment on ProchainADiscuter {
    bibard
    bibardSuffixe
    legislature
    nbrAmdtRestant
    numAmdt
    organeAbrv
  }
`

main.ports.graphqlGetDiscussion.subscribe(function ({bibard, bibardSuffixe, legislature, organe}) {
  if (!bibardSuffixe) {
    bibardSuffixe = null
  }
  graphqlClient.query({
    query: gql`
      query Discussion($bibard: String!, $bibardSuffixe: String, $legislature: Int!, $organe: String!) {
        discussion(bibard: $bibard, bibardSuffixe: $bibardSuffixe, legislature: $legislature, organe: $organe) {
          ...DiscussionFragment
        }
      }
      ${amendementFragment}
      ${auteurFragment}
      ${cosignatairesMentionLibreFragment}
      ${discussionFragment}
      ${divisionFragment}
      ${enveloppeAmendementFragment}
      ${pageFragment}
    `,
    variables: {
      bibard,
      bibardSuffixe,
      legislature,
      organe
    }
  })
    .then(data => {
      if (!data.data || !data.data.discussion) {
        console.log("graphqlGetDiscussion", data);
      }
      main.ports.gotDiscussion.send(data.data.discussion);
    })
    .catch(error => console.log("graphqlGetDiscussion.catch", error))
});

main.ports.graphqlInit.subscribe(function ({httpUrl, wsUrl}) {
  const link = ApolloLink.split(
    operation => {
      const operationAST = getOperationAST(operation.query, operation.operationName);
      return !!operationAST && operationAST.operation === 'subscription';
    },
    new WebSocketLink({
      uri: wsUrl,
      options: {
        reconnect: true, //auto-reconnect
        // // carry login state (should use secure websockets (wss) when using this)
        // connectionParams: {
        //   authToken: localStorage.getItem("Meteor.loginToken")
        // }
      }
    }),
    new HttpLink({ uri: httpUrl })
  );
  // Finally, create your ApolloClient instance with the modified network interface
  graphqlClient = new ApolloClient({
    cache: new InMemoryCache(),
    link: link
  });
  main.ports.initedGraphql.send(graphqlClient);
});

main.ports.graphqlReset.subscribe(function () {
  graphqlClient.resetStore()
});

main.ports.graphqlSubscribeToAmendementUpserted.subscribe(function ({bibard, bibardSuffixe, legislature, organe}) {
  if (!bibardSuffixe) {
    bibardSuffixe = null
  }
  if (graphqAmendementUpsertedSubscription) {
    graphqAmendementUpsertedSubscription.unsubscribe();
    graphqAmendementUpsertedSubscription = null;
  }
  graphqAmendementUpsertedSubscription = graphqlClient.subscribe({
    query: gql`
      subscription onAmendementUpserted($bibard: String!, $bibardSuffixe: String, $legislature: Int!, $organe: String!) {
        amendementUpserted(bibard: $bibard, bibardSuffixe: $bibardSuffixe, legislature: $legislature, organe: $organe) {
          ...AmendementFragment
        }
      }
      ${amendementFragment}
      ${auteurFragment}
      ${cosignatairesMentionLibreFragment}
    `,
    variables: {
      bibard,
      bibardSuffixe,
      legislature,
      organe
    }
  }).subscribe({
    next (data) {
      if (!data.data) {
        console.log("graphqlSubscribeToAmendementUpserted.subscribe.next", data);
      }
      // Notify your application with the new arrived data
      main.ports.amendementUpserted.send(data.data.amendementUpserted);
    }
  });
});

main.ports.graphqlSubscribeToProchainADiscuterUpserted.subscribe(function () {
  if (graphqProchainADiscuterUpsertedSubscription) {
    graphqProchainADiscuterUpsertedSubscription.unsubscribe();
    graphqProchainADiscuterUpsertedSubscription = null;
  }
  graphqProchainADiscuterUpsertedSubscription = graphqlClient.subscribe({
    query: gql`
      subscription onProchainADiscuterUpserted {
        prochainADiscuterUpserted {
          ...ProchainADiscuterFragment
        }
      }
      ${prochainADiscuterFragment}
    `,
    variables: {
    }
  }).subscribe({
    next (data) {
      if (!data.data) {
        console.log("graphqlSubscribeToProchainADiscuterUpserted.subscribe.next", data);
      }
      // Notify your application with the new arrived data
      main.ports.prochainADiscuterUpserted.send(data.data.prochainADiscuterUpserted);
    }
  });
});


main.ports.setDocumentMetatags.subscribe(function (metatags) {
  if (metatags.hasOwnProperty('description')) {
    var element = document.head.querySelector('meta[property="og:description"]');
    if (element) {
      element.setAttribute('content', metatags.description);
    }
  }

  if (metatags.hasOwnProperty('imageUrl')) {
    var element = document.head.querySelector('meta[property="og:image"]');
    if (element) {
      element.setAttribute('content', metatags.imageUrl);
    }
  }

  if (metatags.hasOwnProperty('title')) {
    var element = document.head.querySelector('meta[property="og:title"]');
    if (element) {
      element.setAttribute('content', metatags.title);
    }

    var elements = document.head.getElementsByTagName('title');
    if (elements.length) {
      var element = elements[0];
      element.innerText = metatags.title;
    }
  }

  var element = document.head.querySelector('meta[property="og:url"]');
  if (element) {
    element.setAttribute('content', window.location.href);
  }

  if (metatags.hasOwnProperty('twitterName')) {
    var element = document.head.querySelector('meta[property="twitter:site"]');
    if (element) {
      element.setAttribute('content', metatags.twitterName);
    }
  }
});
