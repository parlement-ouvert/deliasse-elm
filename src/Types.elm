module Types exposing (..)

import Dict exposing (Dict)


type alias Amendement =
    { auteur : Auteur
    , bibard : String
    , bibardSuffixe : String
    , cosignataires : List Auteur
    , cosignatairesMentionLibre : Maybe CosignatairesMentionLibre
    , dispositif : String
    , etat : Etat
    , exposeSommaire : String
    , legislature : Int
    , numero : String
    , numeroLong : String
    , numeroReference : String
    , organeAbrv : String
    , place : String
    , placeReference : String
    , sortEnSeance : SortEnSeance
    }


type alias Auteur =
    { civilite : String
    , estRapporteur : Bool
    , estGouvernement : Bool
    , groupeTribunId : String
    , nom : String
    , photoUrl : String
    , prenom : String
    , qualite : Maybe String
    , tribunId : String
    }


type alias BillPage =
    { body : String
    , numero : String
    , slug : String
    , titre : Maybe String
    }


type alias CosignatairesMentionLibre =
    { titre : String }


type alias Discussion =
    { amendements : List EnveloppeAmendement
    , bibard : String
    , bibardSuffixe : String
    , divisions : List Division
    , legislature : Int
    , numeroProchainADiscuter : Maybe String
    , organe : String
    , pages : Dict String BillPage
    , titre : String
    , typeDiscussion : TypeDiscussion
    }


type alias Division =
    { -- amendements:
      place : String
    , position : String
    }


type alias EnveloppeAmendement =
    { alineaLabel : String
    , amendement : Maybe Amendement
    , auteurGroupe : Groupe
    , auteurLabel : String
    , discussionCommune : String
    , discussionCommuneAmdtPositon : String
    , discussionCommuneSsAmdtPositon : String
    , discussionIdentique : String
    , discussionIdentiqueAmdtPositon : String
    , discussionIdentiqueSsAmdtPositon : String
    , missionLabel : String
    , numero : String
    , parentNumero : String
    , place : String
    , position : String
    , sort : SortEnSeance
    }


type Etat
    = Accepté
    | Discuté


type Groupe
    = GaucheDémocrateEtRépublicaine
    | LaFranceInsoumise
    | LaRépubliqueEnMarche
    | LesRépublicains
    | MouvementDémocrateEtApparentés
    | NonInscrits
    | NouvelleGauche
    | PasUnGroupe -- Gouvernement...
    | UdiAgirEtIndépendants


type alias ProchainADiscuter =
    { bibard : String
    , bibardSuffixe : String
    , legislature : Int
    , nbrAmdtRestant : Int
    , numAmdt : String
    , organeAbrv : String
    }


type SortEnSeance
    = Adopté
    | Déposé
    | NonSoutenu
    | Rejeté
    | Retiré
    | Tombé


type TypeDiscussion
    = ProjetDeLoi
