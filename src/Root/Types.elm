module Root.Types exposing (..)

import About.Types
import Discussions.Item.Types
import Json.Encode
import Material
import Navigation


type alias Flags =
    {}


type alias Model =
    { location : Navigation.Location
    , mdl : Material.Model
    , page : Page
    }


type Msg
    = AboutMsg About.Types.InternalMsg
    | AmendementUpserted Json.Encode.Value
    | DiscussionMsg Discussions.Item.Types.InternalMsg
    | GraphqlInited Json.Encode.Value
    | LocationChanged Navigation.Location
    | Mdl (Material.Msg Msg)
    | Navigate String
    | ProchainADiscuterUpserted Json.Encode.Value
    | ScrolledToTop


type Page
    = AboutPage About.Types.Model
    | DiscussionPage Discussions.Item.Types.Model
    | HomePage
    | NotFoundPage (List String)


translateAboutMsg : About.Types.MsgTranslator Msg
translateAboutMsg =
    About.Types.translateMsg
        { onInternalMsg = AboutMsg
        , onNavigate = Navigate
        }


translateDiscussionMsg : Discussions.Item.Types.MsgTranslator Msg
translateDiscussionMsg =
    Discussions.Item.Types.translateMsg
        { onInternalMsg = DiscussionMsg
        , onNavigate = Navigate
        }
