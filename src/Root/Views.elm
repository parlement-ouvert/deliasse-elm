module Root.Views exposing (..)

-- import About.Views

import Configuration
import Discussions.Item.Views
import Html exposing (..)
import Html.Attributes exposing (..)
import Material.Layout as Layout
import Material.Options as Options exposing (css, when)
import Root.Types exposing (..)


header : Model -> List (Html Msg)
header model =
    case model.page of
        DiscussionPage discussionModel ->
            Discussions.Item.Views.header discussionModel
                |> List.map (Html.map translateDiscussionMsg)

        _ ->
            [ Layout.row
                [ Options.nop
                , css "transition" "height 333ms ease-in-out 0s"
                ]
                [ Layout.title [] [ text Configuration.appTitle ]
                ]
            ]


view : Model -> Html Msg
view model =
    div []
        [ case model.page of
            AboutPage aboutModel ->
                text ""

            DiscussionPage discussionModel ->
                Discussions.Item.Views.viewLinkedContent discussionModel
                    |> Html.map translateDiscussionMsg

            HomePage ->
                text ""

            NotFoundPage path ->
                text ""
        , Layout.render Mdl
            model.mdl
            [ Layout.fixedHeader
            , Layout.fixedDrawer
            ]
            { header = header model
            , drawer =
                case model.page of
                    AboutPage aboutModel ->
                        []

                    DiscussionPage discussionModel ->
                        Discussions.Item.Views.viewDrawer discussionModel
                            |> List.map (Html.map translateDiscussionMsg)

                    HomePage ->
                        []

                    NotFoundPage path ->
                        []
            , tabs = ( [], [] )
            , main =
                [ case model.page of
                    AboutPage aboutModel ->
                        -- About.Views.view aboutModel
                        --     |> Html.map translateAboutMsg
                        text "TODO: About"

                    DiscussionPage discussionModel ->
                        Discussions.Item.Views.view discussionModel
                            |> Html.map translateDiscussionMsg

                    HomePage ->
                        a [ href "/592//15/AN" ] [ text "Le live du débat sur la protection des données personnelles" ]

                    NotFoundPage path ->
                        text <| "Page non trouvée: " ++ (String.join "/" path)
                ]
            }
        ]
