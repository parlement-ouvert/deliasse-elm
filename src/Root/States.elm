module Root.States exposing (..)

import About.States
import Decoders
import Discussions.Item.States
import Discussions.Item.Types
import Material
import Material.Layout as Layout


-- import Dom.Scroll

import Erl
import Json.Decode
import Navigation
import Ports
import Root.Types exposing (..)
import Routes exposing (..)
import Task
import Urls


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags location =
    { location = location
    , mdl = Material.model
    , page = HomePage
    }
        ! [ Ports.initGraphql
          , Layout.sub0 Mdl
          , Task.perform (\_ -> LocationChanged location) (Task.succeed ())
          ]


subscriptions : Model -> Sub Msg
subscriptions model =
    List.filterMap identity
        [ Just <| Ports.amendementUpserted AmendementUpserted
        , Just <| Ports.initedGraphql GraphqlInited
        , Just <| Ports.prochainADiscuterUpserted ProchainADiscuterUpserted
        , Just <| Layout.subs Mdl model.mdl
        , case model.page of
            AboutPage _ ->
                Nothing

            DiscussionPage discussionModel ->
                Just <| Sub.map DiscussionMsg (Discussions.Item.States.subscriptions discussionModel)

            HomePage ->
                Nothing

            NotFoundPage _ ->
                Nothing
        ]
        |> Sub.batch


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AboutMsg aboutMsg ->
            case model.page of
                AboutPage aboutModel ->
                    let
                        ( updatedAboutModel, aboutCmd ) =
                            About.States.update aboutMsg aboutModel
                    in
                        ( { model | page = AboutPage updatedAboutModel }
                        , Cmd.map translateAboutMsg aboutCmd
                        )

                _ ->
                    ( model, Cmd.none )

        DiscussionMsg discussionMsg ->
            case model.page of
                DiscussionPage discussionModel ->
                    let
                        ( updatedDiscussionModel, discussionCmd ) =
                            Discussions.Item.States.update discussionMsg discussionModel
                    in
                        ( { model | page = DiscussionPage updatedDiscussionModel }
                        , Cmd.map translateDiscussionMsg discussionCmd
                        )

                _ ->
                    ( model, Cmd.none )

        AmendementUpserted amendementJson ->
            case Json.Decode.decodeValue Decoders.amendementDecoder amendementJson of
                Err message ->
                    let
                        _ =
                            Debug.log "AmendementUpserted Decode error:" message

                        _ =
                            Debug.log "AmendementUpserted JSON:" amendementJson
                    in
                        ( model, Cmd.none )

                Ok amendement ->
                    case model.page of
                        DiscussionPage discussionModel ->
                            let
                                ( updatedDiscussionModel, discussionCmd ) =
                                    Discussions.Item.States.update
                                        (Discussions.Item.Types.AmendementUpserted amendement)
                                        discussionModel
                            in
                                ( { model | page = DiscussionPage updatedDiscussionModel }
                                , Cmd.map translateDiscussionMsg discussionCmd
                                )

                        _ ->
                            ( model, Cmd.none )

        GraphqlInited _ ->
            model
                ! [ Ports.subscribeToAmendementUpserted "592" "" 15 "AN"

                  -- , Ports.subscribeToAmendementUpserted "490" "" 15 "CION_LOIS"
                  , Ports.subscribeToProchainADiscuterUpserted
                  ]

        LocationChanged location ->
            urlUpdate location model

        Mdl msg_ ->
            Material.update Mdl msg_ model

        Navigate path ->
            ( model
            , Navigation.newUrl path
            )

        ProchainADiscuterUpserted prochainADiscuterJson ->
            case Json.Decode.decodeValue Decoders.prochainADiscuterDecoder prochainADiscuterJson of
                Err message ->
                    let
                        _ =
                            Debug.log "ProchainADiscuterUpserted Decode error:" message

                        _ =
                            Debug.log "ProchainADiscuterUpserted JSON:" prochainADiscuterJson
                    in
                        ( model, Cmd.none )

                Ok prochainADiscuter ->
                    case model.page of
                        DiscussionPage discussionModel ->
                            let
                                ( updatedDiscussionModel, discussionCmd ) =
                                    Discussions.Item.States.update
                                        (Discussions.Item.Types.ProchainADiscuterUpserted prochainADiscuter)
                                        discussionModel
                            in
                                ( { model | page = DiscussionPage updatedDiscussionModel }
                                , Cmd.map translateDiscussionMsg discussionCmd
                                )

                        _ ->
                            ( model, Cmd.none )

        ScrolledToTop ->
            ( model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    let
        ( newModel, cmd ) =
            case parseLocation location of
                Just AboutRoute ->
                    let
                        newAboutModel =
                            About.States.init

                        aboutModel =
                            case model.page of
                                AboutPage aboutModel ->
                                    if About.States.sameInit newAboutModel aboutModel then
                                        aboutModel
                                    else
                                        newAboutModel

                                _ ->
                                    newAboutModel

                        ( updatedAboutModel, updatedAboutCmd ) =
                            About.States.urlUpdate location aboutModel
                    in
                        ( { model | page = AboutPage updatedAboutModel }
                        , Cmd.map translateAboutMsg updatedAboutCmd
                        )

                Just (DiscussionRoute bibard bibardSuffixe legislature organe discussionRoute) ->
                    let
                        newDiscussionModel =
                            Discussions.Item.States.init bibard bibardSuffixe legislature organe

                        discussionModel =
                            case model.page of
                                DiscussionPage discussionModel ->
                                    if Discussions.Item.States.sameInit newDiscussionModel discussionModel then
                                        discussionModel
                                    else
                                        newDiscussionModel

                                _ ->
                                    newDiscussionModel

                        ( updatedDiscussionModel, updatedDiscussionCmd ) =
                            Discussions.Item.States.urlUpdate location discussionRoute discussionModel
                    in
                        ( { model | page = DiscussionPage updatedDiscussionModel }
                        , Cmd.map translateDiscussionMsg updatedDiscussionCmd
                        )

                Just HomeRoute ->
                    -- ( model, Navigation.newUrl "490//15/CION_LOIS" )
                    ( model, Navigation.newUrl "592//15/AN" )

                Just (NotFoundRoute path) ->
                    ( { model | page = NotFoundPage path }
                    , Ports.setDocumentMetadata
                        { description = "La page demandée n'existe pas."
                        , imageUrl = Urls.appLogoFullUrl
                        , title = "Page non trouvée."
                        }
                    )

                Nothing ->
                    let
                        url =
                            location.href
                                |> Erl.parse

                        newUrl =
                            { url | path = url.path }
                    in
                        ( model, Navigation.modifyUrl (Erl.toString newUrl) )
    in
        { newModel
            | location = location
        }
            ! [ -- Task.attempt
                --     (always (ForSelf <| ScrolledToId))
                --     (Dom.Scroll.toTop "html-element")
                --   ,
                cmd
              ]
