module Decoders exposing (..)

import Dict
import Json.Decode exposing (andThen, bool, Decoder, fail, field, int, list, nullable, oneOf, string, succeed)
import Json.Decode.Extra exposing ((|:))
import Types exposing (..)


amendementDecoder : Decoder Amendement
amendementDecoder =
    succeed Amendement
        |: field "auteur" auteurDecoder
        |: field "bibard" string
        |: field "bibardSuffixe" string
        |: field "cosignataires" (list auteurDecoder)
        |: field "cosignatairesMentionLibre" (nullable cosignatairesMentionLibreDecoder)
        |: oneOf [ field "dispositif" string, succeed "" ]
        |: field "etat" etatDecoder
        |: oneOf [ field "exposeSommaire" string, succeed "" ]
        |: field "legislature" int
        |: field "numero" string
        |: field "numeroLong" string
        |: field "numeroReference" string
        |: field "organeAbrv" string
        |: field "place" string
        |: field "placeReference" string
        |: field "sortEnSeance" sortEnSeanceDecoder


auteurDecoder : Decoder Auteur
auteurDecoder =
    succeed Auteur
        |: field "civilite" string
        |: field "estRapporteur" bool
        |: field "estGouvernement" bool
        |: field "groupeTribunId" string
        |: field "nom" string
        |: field "photoUrl" string
        |: field "prenom" string
        |: field "qualite" (nullable string)
        |: field "tribunId" string


billPageDecoder : Decoder BillPage
billPageDecoder =
    succeed BillPage
        |: field "body" string
        |: field "numero" string
        |: field "slug" string
        |: field "titre" (nullable string)


cosignatairesMentionLibreDecoder : Decoder CosignatairesMentionLibre
cosignatairesMentionLibreDecoder =
    succeed CosignatairesMentionLibre
        |: field "titre" string


discussionDecoder : Decoder Discussion
discussionDecoder =
    let
        billPagesListToDict billPages =
            billPages
                |> List.map (\billPage -> ( billPage.slug, billPage ))
                |> Dict.fromList
    in
        succeed Discussion
            |: field "amendements" (list enveloppeAmendementDecoder)
            |: field "bibard" string
            |: field "bibardSuffixe" string
            |: field "divisions" (list divisionDecoder)
            |: field "legislature" int
            |: field "numeroProchainADiscuter" (nullable string)
            |: field "organe" string
            |: (field "pages" (list billPageDecoder)
                    |> andThen (\billPages -> succeed <| billPagesListToDict billPages)
               )
            |: field "titre" string
            |: field "type" typeDiscussionDecoder


divisionDecoder : Decoder Division
divisionDecoder =
    succeed Division
        -- |: field "amendements" (list enveloppeAmendementDecoder)
        |: field "place" string
        |: field "position" string


enveloppeAmendementDecoder : Decoder EnveloppeAmendement
enveloppeAmendementDecoder =
    succeed EnveloppeAmendement
        |: field "alineaLabel" string
        |: field "amendement" (nullable amendementDecoder)
        |: oneOf [ field "auteurGroupe" groupeDecoder, succeed PasUnGroupe ]
        |: field "auteurLabel" string
        |: field "discussionCommune" string
        |: field "discussionCommuneAmdtPositon" string
        |: field "discussionCommuneSsAmdtPositon" string
        |: field "discussionIdentique" string
        |: field "discussionIdentiqueAmdtPositon" string
        |: field "discussionIdentiqueSsAmdtPositon" string
        |: field "missionLabel" string
        |: field "numero" string
        |: field "parentNumero" string
        |: field "place" string
        |: field "position" string
        |: field "sort" sortEnSeanceDecoder


etatDecoder : Decoder Etat
etatDecoder =
    string
        |> andThen
            (\str ->
                case str of
                    "AC" ->
                        succeed Accepté

                    "DI" ->
                        succeed Discuté

                    somethingElse ->
                        fail <| "Unknown Etat: " ++ somethingElse
            )


groupeDecoder : Decoder Groupe
groupeDecoder =
    string
        |> andThen
            (\str ->
                case str of
                    "FI" ->
                        succeed LaFranceInsoumise

                    "GDR" ->
                        succeed GaucheDémocrateEtRépublicaine

                    "LR" ->
                        succeed LesRépublicains

                    "MODEM" ->
                        succeed MouvementDémocrateEtApparentés

                    "NG" ->
                        succeed NouvelleGauche

                    "NI" ->
                        succeed NonInscrits

                    "REM" ->
                        succeed LaRépubliqueEnMarche

                    somethingElse ->
                        fail <| "Unknown Groupe: " ++ somethingElse
            )


prochainADiscuterDecoder : Decoder ProchainADiscuter
prochainADiscuterDecoder =
    succeed ProchainADiscuter
        |: field "bibard" string
        |: field "bibardSuffixe" string
        |: field "legislature" int
        |: field "nbrAmdtRestant" int
        |: field "numAmdt" string
        |: field "organeAbrv" string


sortEnSeanceDecoder : Decoder SortEnSeance
sortEnSeanceDecoder =
    string
        |> andThen
            (\str ->
                case str of
                    "" ->
                        succeed Déposé

                    "Adopté" ->
                        succeed Adopté

                    "Non soutenu" ->
                        succeed NonSoutenu

                    "Rejeté" ->
                        succeed Rejeté

                    "Retiré" ->
                        succeed Retiré

                    "Tombé" ->
                        succeed Tombé

                    somethingElse ->
                        fail <| "Unknown SortEnSeance: " ++ somethingElse
            )


typeDiscussionDecoder : Decoder TypeDiscussion
typeDiscussionDecoder =
    string
        |> andThen
            (\str ->
                case str of
                    "projet de loi" ->
                        succeed ProjetDeLoi

                    somethingElse ->
                        fail <| "Unknown TypeDiscussion: " ++ somethingElse
            )
