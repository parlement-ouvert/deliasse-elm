port module Ports exposing (..)

import Configuration
import Json.Encode
import Urls


-- DOCUMENT METADATA


type alias DocumentMetadata =
    { description : String
    , imageUrl : String
    , title : String
    }


type alias DocumentMetatags =
    { description : String
    , imageUrl : String
    , title : String
    , twitterName : String
    }


setDocumentMetadata : DocumentMetadata -> Cmd msg
setDocumentMetadata metadata =
    setDocumentMetatags
        { description = metadata.description
        , imageUrl = Urls.fullApiUrl metadata.imageUrl ++ "?dim=500"
        , title = metadata.title ++ " – " ++ Configuration.appTitle
        , twitterName = Configuration.twitterName
        }


port setDocumentMetatags : DocumentMetatags -> Cmd msg



-- GRAPHQL


type alias GraphqlInitArguments =
    { httpUrl : String
    , wsUrl : String
    }


type alias GraphqlAmendementUpsertedArguments =
    { bibard : String
    , bibardSuffixe : String
    , legislature : Int
    , organe : String
    }


type alias GraphqlDiscussionArguments =
    { bibard : String
    , bibardSuffixe : String
    , legislature : Int
    , organe : String
    }


port amendementUpserted : (Json.Encode.Value -> msg) -> Sub msg


port gotDiscussion : (Json.Encode.Value -> msg) -> Sub msg


port graphqlGetDiscussion : GraphqlDiscussionArguments -> Cmd msg


port graphqlInit : GraphqlInitArguments -> Cmd msg


port graphqlReset : String -> Cmd msg


port graphqlSubscribeToAmendementUpserted : GraphqlAmendementUpsertedArguments -> Cmd msg


port graphqlSubscribeToProchainADiscuterUpserted : String -> Cmd msg


port initedGraphql : (Json.Encode.Value -> msg) -> Sub msg


port prochainADiscuterUpserted : (Json.Encode.Value -> msg) -> Sub msg


getDiscussion : String -> String -> Int -> String -> Cmd msg
getDiscussion bibard bibardSuffixe legislature organe =
    graphqlGetDiscussion
        { bibard = bibard
        , bibardSuffixe = bibardSuffixe
        , legislature = legislature
        , organe = organe
        }


initGraphql : Cmd msg
initGraphql =
    graphqlInit
        { httpUrl = Configuration.apiUrl ++ "graphql"
        , wsUrl = "ws" ++ (String.dropLeft 4 Configuration.apiUrl) ++ "subscriptions"
        }


resetGraphql : Cmd msg
resetGraphql =
    graphqlReset "not used"


subscribeToAmendementUpserted : String -> String -> Int -> String -> Cmd msg
subscribeToAmendementUpserted bibard bibardSuffixe legislature organe =
    graphqlSubscribeToAmendementUpserted
        { bibard = bibard
        , bibardSuffixe = bibardSuffixe
        , legislature = legislature
        , organe = organe
        }


subscribeToProchainADiscuterUpserted : Cmd msg
subscribeToProchainADiscuterUpserted =
    graphqlSubscribeToProchainADiscuterUpserted "not used"
