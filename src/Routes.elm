module Routes exposing (..)

import Discussions.Item.Routes
import Navigation
import UrlParser exposing ((</>), int, map, oneOf, parsePath, Parser, remaining, s, string, top)


type Route
    = AboutRoute
    | DiscussionRoute String String Int String Discussions.Item.Routes.Route
    | HomeRoute
    | NotFoundRoute (List String)


discussionRouteParser : Parser (Discussions.Item.Routes.Route -> a) a
discussionRouteParser =
    oneOf
        [ map Discussions.Item.Routes.IndexRoute top
        , map Discussions.Item.Routes.ByGroupeRoute (s "groupes")
        , map Discussions.Item.Routes.FullRoute (s "tout")
        ]


parseLocation : Navigation.Location -> Maybe Route
parseLocation location =
    parsePath routeParser location


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map HomeRoute top
        , map AboutRoute (s "a-propos")
        , map DiscussionRoute (string </> string </> int </> string </> discussionRouteParser)
        , map NotFoundRoute remaining
        ]
