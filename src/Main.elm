module Main exposing (..)

import Navigation
import Root.States
import Root.Types
import Root.Views


main : Program Root.Types.Flags Root.Types.Model Root.Types.Msg
main =
    Navigation.programWithFlags Root.Types.LocationChanged
        { init = Root.States.init
        , update = Root.States.update
        , view = Root.Views.view
        , subscriptions = Root.States.subscriptions
        }
