module About.States exposing (..)

import About.Types exposing (..)
import Navigation
import Ports
import Urls


init : Model
init =
    {}


sameInit : Model -> Model -> Bool
sameInit left right =
    True


update : InternalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate _ model =
    ( model
    , Ports.setDocumentMetadata
        { description = "À propos de Déliasse"
        , imageUrl = Urls.appLogoFullUrl
        , title = "À propos"
        }
    )
